import logo from './logo.svg';
import React, { useState,useEffect } from 'react';
import './App.css';

function App() {

    const [inputValue, setInputValue] = useState('');
    // Mengatur nilai input saat komponen di-render
    useEffect(() => {
      setInputValue(0.1);
    }, []);

    const handleInputChange = (e) => {
      setInputValue(e.target.value);
    };

    const setMax = () => {
      setInputValue(1);
    };

    const handleSubmit = (e) => {
      e.preventDefault();
      // Lakukan sesuatu dengan nilai input yang sudah diambil
      // console.log('Nilai input yang diambil:', inputValue);
      document.getElementById('getValue').innerHTML = inputValue;
    };
    
    
  return (
    <div className="App">
      <header className="App-header">
        <nav>
          <div style={{display:'flex',float:'right'}}>
            <img src='https://phantasmrealm.com/wp-content/uploads/2023/07/ERA-white.png' style={{height: '100%',margin: 'auto 30px'}}/>
            <button onclick="window.location.href='https://phantasmrealm.com/explore-the-realm/#hex'" class="shiny2"> Connect Wallet</button>
          </div>
        </nav>

        <img src="https://phantasmrealm.com/wp-content/uploads/2023/07/MAINLOGO_UPDATED-1.png" alt="logo" />
        <p className='description my-8'>
          Phantasm is a distinctive fusion of DEX protocols with on-chain mythical game where you can utilize your cryptocurrencies and boost with NFT to increase your rewards.
        </p>

        <div className="grid grid-cols-3 gap-3 bg-transparant mb-10 wd-80">
          <div className='raised'>
            <img src="https://phantasmrealm.com/wp-content/uploads/2023/07/Target.png" />
            <h3>Target Raised<br/><strong>100 ETH</strong></h3>
          </div>
          <div className='raised'>
            <img src="https://phantasmrealm.com/wp-content/uploads/2023/07/Bangungan.png" />
            <h3>Total Raised<br/><strong>100 ETH</strong></h3>
          </div>
          <div className='raised'>
            <img src="https://phantasmrealm.com/wp-content/uploads/2023/07/COIN_PTSM_LOGO-1.png" />
            <h3>PRICE<br/><strong>$0.001</strong></h3>
          </div>          
        </div>

        <div className="grid grid-cols-2 gap-10 wd-80">
          <div className="grid grid-cols-1 gap-1 bg-transparant-nopadding">
            <div className='p-3 raised-header'>MAIN EVENT DETAILS</div>
            <div className="grid grid-cols-2 gap-2 p-5">
              <div className='raised-desc'>
                <h3>05/06/2023<br></br>15:00 UTC<br></br><br></br>Soft Cap :<br></br>50 ETH</h3>
              </div>
              <div className='raised-desc'>
                <h3>End Date 06/06/2023<br></br>15:00 UTC<br></br><br></br>Hard Cap :<br></br>150 ETH</h3>
              </div>
            </div>
          </div>

          <div className="grid grid-cols-1 gap-1 bg-transparant-nopadding">
            <form onSubmit={handleSubmit}>
            <div className='p-3 raised-header'>SALE</div>
              <div className='form-group'>
                <input type="number"
                  value={inputValue}
                  onChange={handleInputChange} min={0.1} step={0.1} max={1} maxLength={1} className='form-input' id="sale" placeholder="Masukkan teks di sini..."/>
                <button className='pull-right btn-submit' type='button' onClick={setMax}>MAX</button>
              </div>
            <div className="grid grid-cols-2 gap-2 p-5">
              <div className='raised-desc'>
                <h3>
                  Min Allocation :<br/><br/>
                  MAx Allocation :<br/><br/>
                  Your Contribution :<br/><br/>
                </h3>
              </div>
              <div className='raised-desc pull-right'>
                <h3>
                  0.1<br/><br/>
                  1<br/><br/>
                  <b id='getValue'>...</b><br/><br/>
                </h3>
              </div>
            </div>

            <button className='btn-submit text-center' type="submit">SUBMIT</button>
            </form>


          </div>
        </div>
      </header>
    </div>
  );
}

export default App;
